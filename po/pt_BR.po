# Brazilian Portuguese translation of gnome-system-log.
# Copyright (C) 1999-2014 the gnome-system-log authors.
# This file is distributed under the same license as the gnome-system-log package.
# Alexandre Hautequest <hquest@fesppr.br>, 1999.
# Ariel Bressan da Silva <ariel@conectiva.com.br>, 2000.
# Francisco Petrúcio Cavalcante Junior <fpcj@impa.br>, 2001.
# Evandro Fernandes Giovanini <evandrofg@ig.com.br>, 2002.
# Welther José O. Esteves <weltherjoe@yahoo.com.br>, 2004.
# Afonso Celso Medina <medina@maua.br>, 2005.
# Guilherme de S. Pastore <gpastore@colband.com.br>, 2005-2006.
# Luiz Fernando S. Armesto <luiz.armesto@gmail.com>, 2007.
# Leonardo Ferreira Fontenelle <leonardof@gnome.org>, 2007-2009.
# Og Maciel <ogmaciel@gnome.org>, 2007-2008.
# Rodrigo Flores <rodrigomarquesflores@gmail.com>, 2007.
# Hugo Doria <hugodoria@gmail.com>, 2007, 2008.
# Djavan Fagundes <dnoway@gmail.com>, 2008, 2009.
# Krix Apolinário <krixapolinario@gmail.com>, 2009.
# Antonio Fernandes C. Neto <fernandesn@gnome.org>, 2010.
# Rodrigo Padula de Oliveira <contato@rodrigopadula.com>, 2011.
# André Gondim <In Memoriam>, 2011.
# Enrico Nicoletto <liverig@gmail.com>, 2012, 2013.
# Rafael Ferreira <rafael.f.f1@gmail.com>, 2013, 2014.
# Fábio Nogueira <deb-user-ba@ubuntu.com>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-utils\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"system-log&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2016-02-29 22:29+0000\n"
"PO-Revision-Date: 2016-02-29 22:41-0300\n"
"Last-Translator: Rafael Fontenelle <rffontenelle@gmail.com>\n"
"Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 1.8.7\n"

#: ../data/gnome-system-log.desktop.in.h:1 ../src/logview-app.c:305
#: ../src/logview-window.c:1246
msgid "System Log"
msgstr "Log do sistema"

#: ../data/gnome-system-log.desktop.in.h:2
msgid "View or monitor system log files"
msgstr "Ver ou monitorar arquivos de log do sistema"

#: ../data/gnome-system-log.desktop.in.h:3
msgid "logs;debug;error;"
msgstr "registros;logs;depuração;erro;"

#: ../data/org.gnome.gnome-system-log.appdata.xml.in.h:1
msgid "Log Viewer"
msgstr "Visualizador de log"

#: ../data/org.gnome.gnome-system-log.appdata.xml.in.h:2
msgid "System log viewer for GNOME"
msgstr "Visualizador de log do sistema para o GNOME"

#: ../data/org.gnome.gnome-system-log.appdata.xml.in.h:3
msgid ""
"GNOME System Log viewer is a graphical user interface to view and monitor "
"system log files."
msgstr ""
"O visualizador de logs do sistema do GNOME é uma interface de usuário "
"gráfica par ver e monitorar arquivos de log do sistema."

#: ../data/org.gnome.gnome-system-log.appdata.xml.in.h:4
msgid ""
"This program displays system log files in a friendly way and allows one to "
"filter or search expressions in them."
msgstr ""
"Este programa exibe arquivos de log do sistema em uma forma amigável e "
"permite que se filtre ou pesquise expressões neles."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:1
msgid "Log file to open up on startup"
msgstr "Arquivo de log para abrir ao iniciar"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:2
msgid ""
"Specifies the log file displayed at startup. The default is either /var/adm/"
"messages or /var/log/messages, depending on your operating system."
msgstr ""
"Especifica o arquivo de log exibido ao iniciar. O padrão é ou /var/adm/"
"messages ou /var/log/messages, dependendo do seu sistema operacional."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:3
msgid "Size of the font used to display the log"
msgstr "Tamanho da fonte usada para exibir o log"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:4
msgid ""
"Specifies the size of the fixed-width font used to display the log in the "
"main tree view. The default is taken from the default terminal font size."
msgstr ""
"Especifica o tamanho da fonte de largura fixa usada para exibir o log na "
"principal visão em árvore. O padrão é definido a partir do tamanho padrão da "
"fonte de terminal."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:5
msgid "Height of the main window in pixels"
msgstr "Altura da janela principal, em pixels"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:6
msgid "Specifies the height of the log viewer main window in pixels."
msgstr ""
"Especifica a altura da janela principal do visualizador de log, em pixels."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:7
msgid "Width of the main window in pixels"
msgstr "Largura da janela principal, em pixels"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:8
msgid "Specifies the width of the log viewer main window in pixels."
msgstr ""
"Especifica a largura da janela principal do visualizador de log, em pixels."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:9
msgid "Log files to open up on startup"
msgstr "Arquivo de log para abrir ao iniciar"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:10
msgid ""
"Specifies a list of log files to open up at startup. A default list is "
"created by reading /etc/syslog.conf."
msgstr ""
"Especifica uma lista de logs para abrir ao iniciar. Uma lista padrão é "
"criada ao ler /etc/syslog.conf."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:11
msgid "List of saved filters"
msgstr "Lista de filtros salvos"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:12
msgid "List of saved regexp filters"
msgstr "Lista de filtros de expresões regulares salvos"

#: ../src/logview-about.h:49
msgid ""
"This program is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation; either version 2 of the License, or (at your option) "
"any later version."
msgstr ""
"Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo "
"sob os termos da Licença Pública Geral GNU como publicada pela Free Software "
"Foundation (FSF); na versão 2 da Licença, ou (na sua opinião) qualquer "
"versão."

#: ../src/logview-about.h:53
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""
"Este programa é distribuído na esperança de que seja útil, mas SEM NENHUMA "
"GARANTIA; sem mesmo implicar garantias de COMERCIABILIDADE ou ADAPTAÇÃO A UM "
"PROPÓSITO PARTICULAR.  Veja a Licença Pública Geral GNU (GPL) para mais "
"detalhes. "

#: ../src/logview-about.h:57
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA"
msgstr ""
"Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este "
"programa; se não, escreva para a Fundação do Software Livre (FSF) Inc., 51 "
"Franklin St, Fifth Floor, Boston, MA 02110-1301 USA."

#. translator credits
#: ../src/logview-about.h:63
msgid "translator-credits"
msgstr ""
"Alexandre Hautequest <hquest@fesppr.br>\n"
"Ariel Bressan da Silva <ariel@conectiva.com.br>\n"
"Evandro Fernandes Giovanini <evandrofg@ig.com.br>\n"
"Francisco Petrúcio Cavalcante Junior <fpcj@impa.br>\n"
"Gustavo Maciel Dias Vieira <gustavo@sagui.org>\n"
"Welther José O. Esteves <weltherjoe@yahoo.com.br>\n"
"Goedson Teixeira Paixão <goedson@debian.org>\n"
"Guilherme de S. Pastore <gpastore@colband.com.br>\n"
"Luiz Fernando S. Armesto <luiz.armesto@gmail.com>\n"
"Leonardo Ferreira Fontenelle <leonardof@gnome.org>\n"
"Og Maciel <ogmaciel@gnome.org>\n"
"Hugo Doria <hugodoria@gmail.com>\n"
"Djavan Fagundes <dnoway@gmail.com>\n"
"Krix Apolinário <krixapolinario@gmail.com>\n"
"Antonio Fernandes C. Neto <fernandesn@gnome.org>\n"
"Rodrigo Padula de Oliveira <contato@rodrigopadula.com>\n"
"André Gondim (In memoriam) <In Memoriam>\n"
"Enrico Nicoletto <liverig@gmail.com>"

#: ../src/logview-app.c:288
#, c-format
msgid "There was an error displaying help: %s"
msgstr "Ocorreu um erro ao exibir a ajuda: %s"

#: ../src/logview-app.c:310 ../src/logview-app.c:396
msgid "A system log viewer for GNOME."
msgstr "Um visualizador de log para o GNOME."

#: ../src/logview-app.c:389
msgid "Show the version of the program."
msgstr "Mostra a versão do programa."

#: ../src/logview-app.c:390
msgid "[URI...]"
msgstr "[URI...]"

#. Translators: this is a fatal error quit message printed on the
#. * command line
#: ../src/logview-app.c:406
msgid "Could not parse arguments"
msgstr "Não foi possível analisar argumentos"

#. Translators: this is a fatal error quit message printed on the
#. * command line
#: ../src/logview-app.c:423
msgid "Could not register the application"
msgstr "Não foi possível registrar o aplicativo"

#: ../src/logview-app.c:531
#, c-format
msgid "Impossible to open the file %s"
msgstr "Impossível abrir o arquivo %s"

#: ../src/logview-app-menu.ui.h:1
msgid "Auto Scroll"
msgstr "Rolagem automática"

#: ../src/logview-app-menu.ui.h:2
msgid "Help"
msgstr "Ajuda"

#: ../src/logview-app-menu.ui.h:3
msgid "About"
msgstr "Sobre"

#: ../src/logview-app-menu.ui.h:4
msgid "Quit"
msgstr "Sair"

#: ../src/logview-filter-manager.c:92
msgid "Filter name is empty!"
msgstr "O nome do filtro está vazio!"

#: ../src/logview-filter-manager.c:105
msgid "Filter name may not contain the ':' character"
msgstr "O nome do filtro não pode conter o caractere \":\""

#: ../src/logview-filter-manager.c:128
msgid "Regular expression is empty!"
msgstr "A expressão regular está vazia!"

#: ../src/logview-filter-manager.c:144
#, c-format
msgid "Regular expression is invalid: %s"
msgstr "Expressão regular inválida: %s"

#: ../src/logview-filter-manager.c:238
msgid "Please specify either foreground or background color!"
msgstr ""
"Por favor especifique tanto a cor do primeiro plano quanto a do plano de "
"fundo!"

#: ../src/logview-filter-manager.c:292
msgid "Edit filter"
msgstr "Editar filtro"

#: ../src/logview-filter-manager.c:292
msgid "Add new filter"
msgstr "Adicionar novo filtro"

#: ../src/logview-filter-manager.c:467 ../src/logview-window.c:1276
msgid "_Close"
msgstr "Fe_char"

#: ../src/logview-filter-manager.c:501
msgid "_Add"
msgstr "_Adicionar"

#: ../src/logview-filter-manager.c:502
msgid "_Properties"
msgstr "_Propriedades"

#: ../src/logview-filter-manager.c:503
msgid "_Remove"
msgstr "_Remover"

#: ../src/logview-filter-manager.c:506 ../src/logview-window.ui.h:8
msgid "Filters"
msgstr "Filtros"

#: ../src/logview-filter.ui.h:1
msgid "_Name:"
msgstr "_Nome:"

#: ../src/logview-filter.ui.h:2
msgid "_Regular Expression:"
msgstr "Expressão _regular:"

#: ../src/logview-filter.ui.h:3
msgid "Highlight"
msgstr "Destaque"

#: ../src/logview-filter.ui.h:4
msgid "Hide"
msgstr "Oculto"

#: ../src/logview-filter.ui.h:5
msgid "Foreground:"
msgstr "Primeiro plano:"

#: ../src/logview-filter.ui.h:6
msgid "Background:"
msgstr "Plano de fundo:"

#: ../src/logview-filter.ui.h:7
msgid "Effect:"
msgstr "Efeito:"

#: ../src/logview-findbar.c:152
msgid "Find previous occurrence of the search string"
msgstr "Localizar ocorrência anterior da expressão pesquisada"

#: ../src/logview-findbar.c:162
msgid "Find next occurrence of the search string"
msgstr "Localizar próxima ocorrência da expressão pesquisada"

#: ../src/logview-log.c:596
msgid "Error while uncompressing the GZipped log. The file might be corrupt."
msgstr ""
"Erro ao descompactar um arquivo de log compactado com o GZip. O arquivo pode "
"estar corrompido."

#: ../src/logview-log.c:643
msgid "You don't have enough permissions to read the file."
msgstr "Você não tem permissões suficientes para ler o arquivo."

#: ../src/logview-log.c:658
msgid "The file is not a regular file or is not a text file."
msgstr "Este arquivo não é um arquivo comum ou não é um arquivo texto."

#: ../src/logview-log.c:740
msgid "This version of System Log does not support GZipped logs."
msgstr ""
"Esta versão do sistema de logs não suporta logs compactados com o GZip."

#: ../src/logview-loglist.c:316
msgid "Loading..."
msgstr "Carregando..."

#: ../src/logview-utils.c:295
msgid "today"
msgstr "hoje"

#: ../src/logview-utils.c:297
msgid "yesterday"
msgstr "ontem"

#: ../src/logview-window.c:178 ../src/logview-window.c:374
#, c-format
msgid "Search in \"%s\""
msgstr "Pesquisar em \"%s\""

#. translators: this is part of a label composed with
#. * a date string, for example "updated today 23:54"
#.
#: ../src/logview-window.c:198
msgid "updated"
msgstr "atualizado"

#: ../src/logview-window.c:342
msgid "Wrapped"
msgstr "Quebrado"

#: ../src/logview-window.c:357
msgid "No matches found"
msgstr "Não foram encontradas ocorrências"

#: ../src/logview-window.c:737
#, c-format
msgid "Can't read from \"%s\""
msgstr "Não é possível ler \"%s\""

#: ../src/logview-window.c:1137
msgid "Open Log"
msgstr "Abrir log"

#: ../src/logview-window.c:1140
msgid "_Cancel"
msgstr "_Cancelar"

#: ../src/logview-window.c:1141
msgid "_Open"
msgstr "_Abrir"

#: ../src/logview-window.c:1370
msgid "Could not open the following files:"
msgstr "Não foi possível abrir os seguintes arquivos:"

#: ../src/logview-window.ui.h:1
msgid "Open..."
msgstr "Abrir..."

#: ../src/logview-window.ui.h:2
msgid "Close"
msgstr "Fechar"

#: ../src/logview-window.ui.h:3
msgid "Copy"
msgstr "Copiar"

#: ../src/logview-window.ui.h:4
msgid "Select All"
msgstr "Selecionar tudo"

#: ../src/logview-window.ui.h:5
msgid "Zoom In"
msgstr "Ampliar"

#: ../src/logview-window.ui.h:6
msgid "Zoom Out"
msgstr "Reduzir"

#: ../src/logview-window.ui.h:7
msgid "Normal Size"
msgstr "Tamanho normal"

#: ../src/logview-window.ui.h:9
msgid "Show Matches Only"
msgstr "Mostrar somente ocorrências"

#: ../src/logview-window.ui.h:10
msgid "Manage Filters..."
msgstr "Gerenciar filtros..."

#~ msgid "About System Log"
#~ msgstr "Sobre o Log do sistema"

#~ msgid "Log File Viewer"
#~ msgstr "Visualizador de arquivos de log"

#~ msgid "_Find:"
#~ msgstr "_Localizar:"

#~ msgid "Find Previous"
#~ msgstr "Localizar anterior"

#~ msgid "Find Next"
#~ msgstr "Localizar próxima"

#~ msgid "Clear the search string"
#~ msgstr "Limpar a expressão de pesquisa"

#~ msgid "Show the application's version"
#~ msgstr "Mostra a versão do aplicativo"

#~ msgid " - Browse and monitor logs"
#~ msgstr " - Navegar e monitorar logs"

#~ msgid "last update: %s"
#~ msgstr "última atualização: %s"

#~ msgid "%d lines (%s) - %s"
#~ msgstr "%d linhas (%s) - %s"

#~ msgid "Not found"
#~ msgstr "Não localizado"

#~ msgid "_File"
#~ msgstr "_Arquivo"

#~ msgid "_Edit"
#~ msgstr "_Editar"

#~ msgid "_View"
#~ msgstr "_Ver"

#~ msgid "_Filters"
#~ msgstr "_Filtros"

#~ msgid "Open a log from file"
#~ msgstr "Abrir um log a partir de um arquivo"

#~ msgid "Close this log"
#~ msgstr "Fecha este log"

#~ msgid "Quit the log viewer"
#~ msgstr "Sai do visualizador de log"

#~ msgid "Copy the selection"
#~ msgstr "Copia a seleção"

#~ msgid "Select the entire log"
#~ msgstr "Seleciona todo o log"

#~ msgid "_Find..."
#~ msgstr "_Localizar..."

#~ msgid "Find a word or phrase in the log"
#~ msgstr "Localiza uma palavra ou frase no arquivo de log"

#~ msgid "Bigger text size"
#~ msgstr "Tamanho de texto maior"

#~ msgid "Smaller text size"
#~ msgstr "Tamanho de texto menor"

#~ msgid "Manage filters"
#~ msgstr "Gerenciar filtros"

#~ msgid "_Contents"
#~ msgstr "Su_mário"

#~ msgid "Open the help contents for the log viewer"
#~ msgstr "Abre o sumário da ajuda para o visualizador de log"

#~ msgid "Show the about dialog for the log viewer"
#~ msgstr "Mostra o diálogo \"sobre\" para o visualizador de log"

#~ msgid "_Statusbar"
#~ msgstr "Barra de _status"

#~ msgid "Show Status Bar"
#~ msgstr "Mostra a barra de status"

#~ msgid "Side _Pane"
#~ msgstr "_Painel lateral"

#~ msgid "Show Side Pane"
#~ msgstr "Mostrar painel lateral"

#~ msgid "Only show lines that match one of the given filters"
#~ msgstr "Somente mostrar linhas que combinam com um dos filtros fornecidos"

#~ msgid "Automatically scroll down when new lines appear"
#~ msgstr "Rolar automaticamente para baixo quando surgirem novas linhas"

#~ msgid "Version: "
#~ msgstr "Versão: "
