# translation of te.po to Telugu
# Telugu translation of gnome-utils.
# Copyright (C) 2007,2012 Swecha Telugu Localisation Team <localisation@swecha.org>
# This file is distributed under the same license as the gnome-utils package.
#
#
#
# Pramod <pramodfsf@gmail.com>, 2007.
# Krishna Babu K <kkrothap@redhat.com>, 2007, 2009, 2010, 2012, 2013, 2014.
# Bhuvan Krishna <bhuvan@swecha.net>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: te\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"system-log&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2014-09-09 07:40+0000\n"
"PO-Revision-Date: 2014-09-09 16:11+0530\n"
"Last-Translator: Krishnababu Krothapalli <kkrothap@redhat.com>\n"
"Language-Team: Telugu <Fedora-trans-te@redhat.com>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"
"\n"
"\n"
"X-Generator: Lokalize 1.5\n"

#: ../data/gnome-system-log.desktop.in.in.h:1 ../src/logview-app.c:305
#: ../src/logview-window.c:1246
msgid "System Log"
msgstr "వ్యవస్థ Log"

#: ../data/gnome-system-log.desktop.in.in.h:2
msgid "View or monitor system log files"
msgstr "దర్శించు లేదా దర్శిని వ్యవస్థ log దస్త్రములు"

#: ../data/gnome-system-log.desktop.in.in.h:3
msgid "logs;debug;error;"
msgstr "logs;debug;error;"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:1
msgid "Log file to open up on startup"
msgstr "మొదలు పెట్టినపుడు log దస్త్రం ను తెరుచుటకు"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:2
msgid ""
"Specifies the log file displayed at startup. The default is either /var/adm/"
"messages or /var/log/messages, depending on your operating system."
msgstr ""
"ప్రారంభంనందు ప్రదర్శితమయ్యే లాగ్ దస్త్రాన్ని తెలియజేస్తుంది.అప్రమేయమనునది మీ "
"ఆపరేటింగ్ సిస్టమ్ పై ఆధారపడి, /"
"var/adm/messages కాని లేదా /var/log/messages కాని అవుతుంది."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:3
msgid "Size of the font used to display the log"
msgstr "అక్షరశైలి యొక్క పరిమాణము log ను దర్శించుటకు వాడబడినది"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:4
msgid ""
"Specifies the size of the fixed-width font used to display the log in the "
"main tree view. The default is taken from the default terminal font size."
msgstr ""
"లాగ్ ను ప్రధాన ట్రీ దర్శిని లో ప్రదర్శించుటకు ఉపయోగించు నిర్దేశిత-వెడల్పు "
"ఫాంటు యొక్క పరిమాణంను "
"తెలియజేస్తుంది.అప్రమేయం అప్రమేయ టెర్మినల్ ఫాంటు పరిమాణంనుండి తీసుకోబడుతుంది."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:5
msgid "Height of the main window in pixels"
msgstr "పిక్సెల్స్ లో మూల గవాక్షం యొక్క ఎత్తు"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:6
msgid "Specifies the height of the log viewer main window in pixels."
msgstr "పిక్సెల్స్ లో మూల గవాక్షం log దర్శకి యొక్క ఎత్తును నిర్థేశిస్తుంది."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:7
msgid "Width of the main window in pixels"
msgstr "పిక్సెల్స్ లో మూల గవాక్షం యొక్క పొడవు"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:8
msgid "Specifies the width of the log viewer main window in pixels."
msgstr "పిక్సెల్స్ లో మూల గవాక్షం log దర్శకి యొక్క పొడవును నిర్థేశిస్తుంది."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:9
msgid "Log files to open up on startup"
msgstr "మొదలు పెట్టినపుడు log దస్త్రాల ను తెరుచుటకు"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:10
msgid ""
"Specifies a list of log files to open up at startup. A default list is "
"created by reading /etc/syslog.conf."
msgstr ""
"ప్రారంభం నందు తెరువుటకు లాగ్ దస్త్రముల జాబితాను తెలియపరచండి.అప్రమేయ జాబితా "
"/etc/syslog.conf "
"చదువుట ద్వారా సృష్టించబడుతుంది."

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:11
msgid "List of saved filters"
msgstr "దాచివున్న  వడపోతుల జాబిత"

#: ../data/org.gnome.gnome-system-log.gschema.xml.in.h:12
msgid "List of saved regexp filters"
msgstr "దాచివున్న  regexp వడపోతుల జాబిత"

#: ../src/logview-about.h:49
msgid ""
"This program is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation; either version 2 of the License, or (at your option) "
"any later version."
msgstr ""
"ఈ ప్రోగ్రామ్ ఉచిత సాఫ్టువేరు; మీరు దానిని ఉచిత సాఫ్టువేరు సంస్థ ద్వారా "
"ప్రచురించబడిన GNU జనరల్ పబ్లిక్ "
"లైసెన్సు వర్షన్ 2, లేదా (మీ ఐచ్చికము వద్ద) దాని తర్వాతి వర్షన్‌కు లోబడి "
"పునఃపంపిణి చేయవచ్చును మరియు/లేదా "
"సవరించవచ్చును;"

#: ../src/logview-about.h:53
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""
"ఈ ప్రోగ్రామ్ అది వుపయోగపడుతుంది అనే వుద్దేశ్యముతో పంపిణి చేయబడింది, అయితే "
"ఎటువంటి హామి లేదు; కనీసం "
"వ్యాపారపరంగా లేదా ఫలానా ప్రయోజనం కొరకు ప్రత్యేకించి అనికూడా లేదు.  మరింత "
"సమాచారము కొరకు GNU జనరల్ "
"పబ్లిక్ లైసెన్సును చూడండి."

#: ../src/logview-about.h:57
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA"
msgstr ""
"ఈ ప్రోగ్రామ్‌తో మీరు వొక GNU జనరల్ పబ్లిక్ లైసెన్సు నకలును కూడా పొందివుంటారు; "
"పొందకపోతే, Free "
"Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  "
"02110-1301, USAకు వ్రాయండి."

#. translator credits
#: ../src/logview-about.h:63
msgid "translator-credits"
msgstr ""
"Pramod <pramodfsf@gmail.com>\n"
"Krishna Babu K <kkrothap@redhat.com>"

#: ../src/logview-app.c:288
#, c-format
msgid "There was an error displaying help: %s"
msgstr "సహాయమును ప్రదర్శించుటలో అక్కడ వొక దోషమువుంది: %s"

#: ../src/logview-app.c:310 ../src/logview-app.c:396
msgid "A system log viewer for GNOME."
msgstr "వ్యవస్థ Log దర్శకి గ్నోమ్ కొరకు."

#: ../src/logview-app.c:389
msgid "Show the version of the program."
msgstr "కార్యక్రమం యొక్క రూపాంతరాన్ని చూపించు."

#: ../src/logview-app.c:390
#| msgid "[LOGFILE...]"
msgid "[URI...]"
msgstr "[URI...]"

#. Translators: this is a fatal error quit message printed on the
#. * command line
#: ../src/logview-app.c:406
msgid "Could not parse arguments"
msgstr "పరామితులు పార్స్ చేయలేకపోయింది"

#. Translators: this is a fatal error quit message printed on the
#. * command line
#: ../src/logview-app.c:423
msgid "Could not register the application"
msgstr "అనువర్తనాన్ని నమోదు చేయలేకపోయింది"

#: ../src/logview-app.c:531
#, c-format
msgid "Impossible to open the file %s"
msgstr "దస్త్రము %sను తెరువుట అసాధ్యము"

#: ../src/logview-app-menu.ui.h:1
msgid "Auto Scroll"
msgstr "ఆటో స్క్రోల్"

#: ../src/logview-app-menu.ui.h:2
msgid "Help"
msgstr "సహాయం"

#: ../src/logview-app-menu.ui.h:3
#| msgid "_About"
msgid "About"
msgstr "గురించి"

#: ../src/logview-app-menu.ui.h:4
msgid "Quit"
msgstr "నిష్క్రమించు"

#: ../src/logview-filter-manager.c:92
msgid "Filter name is empty!"
msgstr "వడపోత నామము ఖాళీగావుంది!"

#: ../src/logview-filter-manager.c:105
msgid "Filter name may not contain the ':' character"
msgstr "వడపోత నామము ':' అక్షరమును కలిగివుండక పోవచ్చును"

#: ../src/logview-filter-manager.c:128
msgid "Regular expression is empty!"
msgstr "సాధారణ సమీకరణం ఖాళీగా వుంది!"

#: ../src/logview-filter-manager.c:144
#, c-format
msgid "Regular expression is invalid: %s"
msgstr "సాధారణ సమీకరణం చెల్లనిది: %s"

#: ../src/logview-filter-manager.c:238
msgid "Please specify either foreground or background color!"
msgstr "దయచేసి ఫోర్‌గ్రౌండ్ లేదా బ్యాక్‌గ్రౌండ్ రంగును తెలుపండి!"

#: ../src/logview-filter-manager.c:292
msgid "Edit filter"
msgstr "వడపోతను సరికూర్చుము"

#: ../src/logview-filter-manager.c:292
msgid "Add new filter"
msgstr "కొత్త వడపోతను జతచేయి"

#: ../src/logview-filter-manager.c:467 ../src/logview-window.c:1276
#| msgid "Close"
msgid "_Close"
msgstr "మూసివేయి (_C)"

#: ../src/logview-filter-manager.c:501
msgid "_Add"
msgstr "జతచేయు(_A)"

#: ../src/logview-filter-manager.c:502
msgid "_Properties"
msgstr "లక్షణాలు (_P)"

#: ../src/logview-filter-manager.c:503
msgid "_Remove"
msgstr "తీసివేయి (_R)"

#: ../src/logview-filter-manager.c:506 ../src/logview-window.ui.h:8
msgid "Filters"
msgstr "వడపోతలు"

#: ../src/logview-filter.ui.h:1
msgid "_Name:"
msgstr "నామము(_N):"

#: ../src/logview-filter.ui.h:2
msgid "_Regular Expression:"
msgstr "క్రమబద్ద వ్యక్తీకరణ_R:"

#: ../src/logview-filter.ui.h:3
msgid "Highlight"
msgstr "ఉద్దేపించు"

#: ../src/logview-filter.ui.h:4
msgid "Hide"
msgstr "దాచు"

#: ../src/logview-filter.ui.h:5
msgid "Foreground:"
msgstr "దృశ్యరంగం:"

#: ../src/logview-filter.ui.h:6
msgid "Background:"
msgstr "పూర్వరంగం:"

#: ../src/logview-filter.ui.h:7
msgid "Effect:"
msgstr "పర్యవసానం:"

#: ../src/logview-findbar.c:152
msgid "Find previous occurrence of the search string"
msgstr "వెతుకుమాట యొక్క సంభవించునది ముందుది వెతుకు"

#: ../src/logview-findbar.c:162
msgid "Find next occurrence of the search string"
msgstr "వెతుకుమాట యొక్క సంభవించునది తరువాత వెతుకు"

#: ../src/logview-log.c:596
msgid "Error while uncompressing the GZipped log. The file might be corrupt."
msgstr ""
"GZipped లాగ్‌ను విడమర్చుతుంటే(అన్‌కంప్రెస్) దోషము. బహుశా దస్త్రము "
"పాడైవుంటుంది."

#: ../src/logview-log.c:643
msgid "You don't have enough permissions to read the file."
msgstr "దస్త్రమును చదువుటకు మీరు తగినన్ని అనుమతులు కలిగిలేరు."

#: ../src/logview-log.c:658
msgid "The file is not a regular file or is not a text file."
msgstr "దస్త్రము సాదారణ దస్త్రముకాదు లేదా టెక్స్ట్ దస్త్రముకాదు."

#: ../src/logview-log.c:740
msgid "This version of System Log does not support GZipped logs."
msgstr "సిస్టమ్ లాగ్‌యొక్క ఈ వర్షన్ GZipped లాగ్‌లను మద్దతించదు."

#: ../src/logview-loglist.c:316
msgid "Loading..."
msgstr "లోడవుచున్నది..."

#: ../src/logview-utils.c:295
msgid "today"
msgstr "ఈ రోజు"

#: ../src/logview-utils.c:297
msgid "yesterday"
msgstr "నిన్న"

#: ../src/logview-window.c:178 ../src/logview-window.c:374
#, c-format
msgid "Search in \"%s\""
msgstr "\"%s\" నందు శోధిస్తోంది"

#. translators: this is part of a label composed with
#. * a date string, for example "updated today 23:54"
#.
#: ../src/logview-window.c:198
msgid "updated"
msgstr "నవీకరించింది"

#: ../src/logview-window.c:342
msgid "Wrapped"
msgstr "పొట్లం కట్టబడిన"

#: ../src/logview-window.c:357
msgid "No matches found"
msgstr "ఏ జోడీ కనబడలేదు"

#: ../src/logview-window.c:737
#, c-format
msgid "Can't read from \"%s\""
msgstr "\"%s\" నుండి చదవలేకపోతున్న"

#: ../src/logview-window.c:1137
msgid "Open Log"
msgstr "log తెరువుము"

#: ../src/logview-window.c:1140
msgid "_Cancel"
msgstr "రద్దుచేయి (_C)"

#: ../src/logview-window.c:1141
#| msgid "Open..."
msgid "_Open"
msgstr "తెరువుము (_O)"

#: ../src/logview-window.c:1370
msgid "Could not open the following files:"
msgstr "ఈ క్రింది దస్త్రములను తెరువలేక పోయింది:"

#: ../src/logview-window.ui.h:1
msgid "Open..."
msgstr "తెరచు..."

#: ../src/logview-window.ui.h:2
msgid "Close"
msgstr "మూయి"

#: ../src/logview-window.ui.h:3
msgid "Copy"
msgstr "నకలుతీయు"

#: ../src/logview-window.ui.h:4
msgid "Select All"
msgstr "అన్నింటిని ఎంచుకొను"

#: ../src/logview-window.ui.h:5
msgid "Zoom In"
msgstr "జూమ్ చేయి"

#: ../src/logview-window.ui.h:6
msgid "Zoom Out"
msgstr "జూమ్ తగ్గించు"

#: ../src/logview-window.ui.h:7
msgid "Normal Size"
msgstr "సాధారణ పరిమాణం"

#: ../src/logview-window.ui.h:9
msgid "Show Matches Only"
msgstr "జోడీలను మాత్రమే చూపుము"

#: ../src/logview-window.ui.h:10
msgid "Manage Filters..."
msgstr "వడపోతలను నిర్వహించుము..."

#~ msgid "About System Log"
#~ msgstr "వ్యవస్థ Log గురించి"

#~ msgid "Log File Viewer"
#~ msgstr "లాగ్ దస్త్రపు దర్శిని"

#~ msgid "_Find:"
#~ msgstr "కనుగొనుము (_F):"

#~ msgid "Find Previous"
#~ msgstr "ముందుది కనుగొనుము"

#~ msgid "Find Next"
#~ msgstr "తరువాతది కనుగొనుము"

#~ msgid "Clear the search string"
#~ msgstr "శోధన పదబందం(స్ట్రింగు)ను శుభ్రముచేయుము"

#~ msgid "Show the application's version"
#~ msgstr "కార్యక్షేత్రముల వివరణము చూపుము"

#~ msgid " - Browse and monitor logs"
#~ msgstr "-అన్వేషణ మరియు దర్శిని logs"

#~ msgid "Log Viewer"
#~ msgstr "Log దర్శకి"

#~ msgid "last update: %s"
#~ msgstr "చివరి తాజాపరచు: %s"

#~ msgid "%d lines (%s) - %s"
#~ msgstr "%d లైనులు (%s) - %s"

#~ msgid "Not found"
#~ msgstr "లభించలేదు"

#~ msgid "_File"
#~ msgstr "దస్త్రం(_F)"

#~ msgid "_Edit"
#~ msgstr "సరిచేయు(_E)"

#~ msgid "_View"
#~ msgstr "దర్శనం(_V)"

#~ msgid "_Filters"
#~ msgstr "వడపోతలు (_F)"

#~ msgid "Open a log from file"
#~ msgstr "దస్త్రం నుండి log తెరువుము"

#~ msgid "Close this log"
#~ msgstr "ఈ log మూసివేయు"

#~ msgid "Quit the log viewer"
#~ msgstr "log దర్శకిని త్యజించు"

#~ msgid "Copy the selection"
#~ msgstr "ఎంపికను నకలుతీయు"

#~ msgid "Select the entire log"
#~ msgstr "మొత్తం log ను ఎంచుకొను"

#~ msgid "_Find..."
#~ msgstr "కనుగొనుము (_F)..."

#~ msgid "Find a word or phrase in the log"
#~ msgstr "లాగ్‌నందు పదమును లేదా మాట(ఫ్రేజ్)ను కనుగొనుము"

#~ msgid "Bigger text size"
#~ msgstr "పెద్దదైన పాఠం పరిమాణం"

#~ msgid "Smaller text size"
#~ msgstr "చిన్నదైన పాఠం పరిమాణం"

#~ msgid "Manage filters"
#~ msgstr "వడపోతలను నిర్వహించుము"

#~ msgid "_Contents"
#~ msgstr "సారాలు(_C)"

#~ msgid "Open the help contents for the log viewer"
#~ msgstr "log దర్శకి కొరకు సహాయ సారములను తెరువుము"

#~ msgid "Show the about dialog for the log viewer"
#~ msgstr " log దర్శకి కొరకు గురించి వివరణ చూపుము"

#~ msgid "_Statusbar"
#~ msgstr "సుస్థితి పట్టీ(_S)"

#~ msgid "Show Status Bar"
#~ msgstr "సుస్థితి పట్టీ చూపుము"

#~ msgid "Side _Pane"
#~ msgstr "పక్క ప్యాన్(_P)"

#~ msgid "Show Side Pane"
#~ msgstr "పక్క ప్యాన్ చూపుము"

#~ msgid "Only show lines that match one of the given filters"
#~ msgstr "ఇచ్చిన వడపోతలలో వొక దానిని పోలు పంక్తులను మాత్రమే చూపుము"

#~ msgid "Automatically scroll down when new lines appear"
#~ msgstr "కొత్త వరుస కనిపిస్తే స్వయం చాలకంగ జరుపు"

#~ msgid "Version: "
#~ msgstr "వివరణము:"
